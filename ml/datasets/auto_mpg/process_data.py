import scipy as sci
import numpy as np
import os
import math
import matplotlib.pyplot as plt

def prep_data():
    data = list()
    with open("auto-mpg-raw.data", "r") as f:
        for line in f:
            line_list = line.split()
            line_list = line_list[0:8]
            row = [(x.strip()) for x in line_list]
            data.append(row)
    data = np.array(data)

    print("shape of data is: " + str(np.shape(data)))

    with open("auto-mpg.csv", "w") as f:
        for i in range(0, len(data[:,0])):
            f.write(",".join(data[i,:]))
            f.write("\n")


def process_data():
    data = list()
    with open("auto-mpg.csv", "r") as f:
        for line in f:
            line_list = line.split(",")
            line_list = line_list[0:7]
            row = [float(x.strip()) for x in line_list]
            data.append(row)
    data = np.array(data)
    print("shape of processed data is: " + str(np.shape(data)))
    return data

# min-max normalization:
def normalize_data(data, col_targ):
    data_normalized = np.zeros_like(data)
    for j in range(0, len(data[0,:])):

        if j == col_targ:
            for i in range(0, len(data[:,0])):
                data_normalized[i,j] = data[i,j]
            continue

        min_ = np.amin(data[:,j])
        max_ = np.amax(data[:,j])
        dif = max_ - min_
        if dif == 0:
            for i in range(0, len(data[:,0])):
                data_normalized[i,j] = 0.5
        else:
            for i in range(0, len(data[:,0])):
                data_normalized[i,j] = float(( data[i,j] - min_)) / dif
    return data_normalized

def save_data(filename, data):
    data = np.array(data)
    np.save(filename, data)
    np.savetxt(filename, data)


col_targ = 0
data_processed = process_data()
data_normalized = normalize_data(data_processed, col_targ)
save_data("auto-mpg-normalized", data_normalized)

