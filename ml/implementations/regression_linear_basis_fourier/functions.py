import scipy as sci
import numpy as np
import os
import math
import matplotlib.pyplot as plt

#############################################################
##############    CONSTANTS:   ##############
#############################################################
DATASETS_PATH = "/home/joeliven/Documents/ut/2016SP/cs395_ml/ml/datasets/"
DATASETS = {   "wine_quality":"winequality_white.csv",
                        "auto_mpg":"auto-mpg-normalized.npy",
                        "concrete_compressive_strength":"concrete_compressive_strength_normalized.npy" # doesn't exist yet
                    }
#############################################################

# def load_data_old():
#     orig_path = os.getcwd()
#     data_sets_path = "/home/joeliven/dev/ml/datasets/"
#     wine_quality_data = "wine_quality/"
#     new_path = data_sets_path + wine_quality_data
#     os.chdir(new_path)
#     data_red_wine = list()
#     with open("winequality-red.csv", "r") as f:
#         f.readline()
#         for line in f:
#             line_list = line.split(";")
#             row = [float(x.strip()) for x in line_list]
#             data_red_wine.append(row)
#     data_red_wine = np.array(data_red_wine)

#     data_white_wine = list()
#     with open("winequality-white.csv", "r") as f:
#         f.readline()
#         for line in f:
#             line_list = line.split(";")
#             row = [float(x.strip()) for x in line_list]
#             data_white_wine.append(row)
#     data_white_wine = np.array(data_white_wine)
#     os.chdir(orig_path)
#     return data_red_wine, data_white_wine

#############################################################
##############    FUNCTIONS:   ##############
#############################################################

def load_data(dataset_dir):
    orig_path = os.getcwd()
    new_path = DATASETS_PATH + dataset_dir + "/"
    os.chdir(new_path)
    data = np.load(DATASETS[dataset_dir])

    os.chdir(orig_path)
    return data


def plot_data(X, col_feat, Y):
    plt.scatter(X[:,col_feat], Y, marker='.', color='b')
    plt.show()

def plot_regression(X, col_feat,Y, Yhat, title, plot):
    plt.scatter(X[:,col_feat], Y, marker='.', color='b')
    if plot:
        sort_array = X[:,col_feat].argsort()
        X_sorted = X[sort_array]
        Y_sorted = Y[sort_array]
        Yhat_sorted = Yhat[sort_array]
        plt.plot(X_sorted[:,col_feat], Yhat_sorted, marker='.', color='r')
    else:
        plt.scatter(X[:,col_feat], Yhat, marker='.', color='r')
    plt.title(title)
    plt.xlabel("Feat " + str(col_feat))
    plt.ylabel("MPG")
    plt.show()



def map_one_feat(x, col_feat, n_bases):
    f_x = np.zeros((len(x[:,0]), n_bases))
    for i in range(0,len(x[:,0])):
        for j in range(0, n_bases):
            f_x[i,j] =  math.cos(math.pi * x[i,col_feat] * j)
    return f_x

def compute_weights_OLS(X,Y):
    XtX = np.dot(np.transpose(X),X)
    Xt = np.transpose(X)
    XtX_ = None
    try:
        XtX_ = np.linalg.inv(XtX)
    except:
        print("error, could not find inverse of XtX")
    if XtX_ is None:
        try:
            XtX_ = np.linalg.pinv(XtX, rcond=1e-10)
        except:
            print("error, could not find psuedo-inverse of XtX")

    if XtX_ is not None:
        XtX_Xt = np.dot(XtX_, Xt)
        W = np.dot(XtX_Xt, Y)
    return W

def compute_SSE(Y, Yhat):
    E = Y - Yhat
    SqrdE = E**2
    sse = np.sum(SqrdE)
    sse_avg = float(sse) / len(Y)
    return sse_avg

def compute_AbsErr(Y, Yhat):
    E = Y - Yhat
    AbsE = np.fabs(E)
    err = np.sum(AbsE)
    err_avg = float(err) / len(Y)
    return err_avg

def split_data(data, test_ratio):
    np.random.shuffle(data)
    n = len(data[:,0])
    n_test = int(n * test_ratio)
    n_train = n - n_test
    data_train = data[0:n_train,:]
    data_test = data[n_train:-1,:]
    return data_train, data_test

def map_feats(X, b_bases):
    n_feats = len(X[0,:])
    X_mapped = np.ones((len(X[:,0]), b_bases**n_feats), np.float)
    ctr = np.zeros(n_feats)
    for i in range(0, len(X[:,0])):
        for j in range(0, b_bases**n_feats):
            for feat in range(0, n_feats):
                X_mapped[i,j] *= math.cos(math.pi * X[i,feat] * ctr[feat])
                # X_mapped[i,j] *= math.cos(math.pi * X[i,feat] * (ctr[feat] + 1))
            counter_inc(n_feats, b_bases, ctr)
    return X_mapped


def counter_inc(n_digit, b_base, cur):
    j = n_digit-1
    while j >= 0:
        if cur[j] < b_base-1:
            cur[j] += 1
            return cur
        else:
            cur[j] = 0
            j -= 1

def counter_mod(n_digit, b_base, dec):
    ret = np.zeros(n_digit)
    j = n_digit-1
    while j >= 0:
        ret[j] = dec % b_base
        dec -= ret[j]
        dec /= b_base
        j -=1
    return ret




