import scipy as sci
import numpy as np
import os
import math
import functions as f

#############################################################
##############    SETUP (GLOBAL):   ##############
#############################################################

dataset_dir = "auto_mpg"
col_targ = 0
data = f.load_data(dataset_dir)
test_ratio = .2

print("shape of data is " + str(np.shape(data)))
print("data is ")
print(data)

data_train, data_test = f.split_data(data, test_ratio)
print("shape of data_train is: " + str(np.shape(data_train)))
print("shape of data_test is: " + str(np.shape(data_test)))

X_train = data_train[:,1:]
X_test = data_test[:,1:]
Y_train = data_train[:,0]
Y_test = data_test[:,0]
print("shape of X_train is: " + str(np.shape(X_train)))
print("shape of X_test is: " + str(np.shape(X_test)))
print("shape of Y_train is: " + str(np.shape(Y_train)))
print("shape of Y_test is: " + str(np.shape(Y_test)))


# for feat in range(0, len(X_train[0,:]) ):
#     pass
#     f.plot_data(X_train, feat, Y_train)
#     f.plot_data(X_test, feat, Y_test)


#############################################################
#####    LINEAR REGRESSION USING FOURIER BASIS FUNCS, ONE DIM AT A TIME:   #######
#############################################################
def one_dim():
    for feat in range(0, len(X_train[0,:]) ):
        for n_bases in range(1, 6):

            X_train_mapped = f.map_feats(X_train[:,feat].reshape(len(X_train[:,0]),1), n_bases)
            X_test_mapped = f.map_feats(X_test[:,feat].reshape(len(X_test[:,0]),1), n_bases)

            # X_train_mapped = np.concatenate((X_train_mapped, np.ones((len(X_train_mapped[:,0]), 1), np.float)), axis=1)
            # X_test_mapped = np.concatenate((X_test_mapped, np.ones((len(X_test_mapped[:,0]), 1), np.float)), axis=1)

            W= f.compute_weights_OLS(X_train_mapped,Y_train)
            print("****************************************************\n")
            print("n_bases is: " + str(n_bases) + "\tW is " + str(np.shape(W)))
            print(W)

            Yhat_train = np.dot(X_train_mapped,W)
            Yhat_test = np.dot(X_test_mapped,W)
            print("shape of Yhat_train is: " + str(np.shape(Yhat_train)))
            print("shape of Yhat_test is: " + str(np.shape(Yhat_test)))

            sse_train = f.compute_SSE(Y_train, Yhat_train)
            abs_err_train = f.compute_AbsErr(Y_train, Yhat_train)
            sse_test = f.compute_SSE(Y_test, Yhat_test)
            abs_err_test = f.compute_AbsErr(Y_test, Yhat_test)
            print("sse_train:\t" + str(sse_train))
            print("abser_train:\t" + str(abs_err_train))
            print("sse_test:\t" + str(sse_test))
            print("abser_test:\t" + str(abs_err_test))

            title = "Feat: " + str(feat) + ", n_bases: " + str(n_bases) + ", TRAIN"
            f.plot_regression(X_train, feat, Y_train, Yhat_train, title, True)
            title = "Feat: " + str(feat) + ", n_bases: " + str(n_bases) + ", TEST"
            f.plot_regression(X_test, feat, Y_test, Yhat_test, title, True)

#############################################################
#####    LINEAR REGRESSION USING FOURIER BASIS FUNCS, MULTIDIM, NO INTERAX:   ######
#############################################################
def all_dims_no_interaction(n_bases):
    for feat in range(0, len(X_train[0,:]) ):
        if feat == 0:
            X_train_mapped = f.map_one_feat(X_train, feat, n_bases)
            X_test_mapped = f.map_one_feat(X_test, feat, n_bases)
        else:
            X_train_mapped = np.concatenate((X_train_mapped, f.map_one_feat(X_train, feat, n_bases)), axis=1)
            X_test_mapped = np.concatenate((X_test_mapped, f.map_one_feat(X_test, feat, n_bases)), axis=1)
        print("shape of X_train_mapped is: " + str(np.shape(X_train_mapped)))
        print("shape of X_test_mapped is: " + str(np.shape(X_test_mapped)))

    W= f.compute_weights_OLS(X_train_mapped,Y_train)
    print("****************************************************\n")
    print("n_bases is: " + str(n_bases) + "\tW is " + str(np.shape(W)))
    print(W)

    Yhat_train = np.dot(X_train_mapped,W)
    Yhat_test = np.dot(X_test_mapped,W)
    print("shape of Yhat_train is: " + str(np.shape(Yhat_train)))
    print("shape of Yhat_test is: " + str(np.shape(Yhat_test)))

    sse_train = f.compute_SSE(Y_train, Yhat_train)
    abs_err_train = f.compute_AbsErr(Y_train, Yhat_train)
    sse_test = f.compute_SSE(Y_test, Yhat_test)
    abs_err_test = f.compute_AbsErr(Y_test, Yhat_test)
    print("sse_train:\t" + str(sse_train))
    print("abser_train:\t" + str(abs_err_train))
    print("sse_test:\t" + str(sse_test))
    print("abser_test:\t" + str(abs_err_test))


#############################################################
#####    LINEAR REGRESSION USING FOURIER BASIS FUNCS, MULTIDIM, W INTERAX:   ######
#############################################################
def all_dims(b_bases):
    X_train_mapped = f.map_feats(X_train, b_bases)
    X_test_mapped = f.map_feats(X_test, b_bases)
    # # add an extra feature always equal to 1 to account for the intercept
    # X_train_mapped = np.concatenate((X_train_mapped, np.ones((len(X_train_mapped[:,0]), 1), np.float)), axis=1)
    # X_test_mapped = np.concatenate((X_test_mapped, np.ones((len(X_test_mapped[:,0]), 1), np.float)), axis=1)

    print("shape of X_train_mapped is: " + str(np.shape(X_train_mapped)))
    print("shape of X_test_mapped is: " + str(np.shape(X_test_mapped)))

    W= f.compute_weights_OLS(X_train_mapped,Y_train)
    print("****************************************************\n")
    print("b_bases is: " + str(b_bases) + "\tW is " + str(np.shape(W)))
    print(W)

    Yhat_train = np.dot(X_train_mapped,W)
    Yhat_test = np.dot(X_test_mapped,W)
    print("shape of Yhat_train is: " + str(np.shape(Yhat_train)))
    print("shape of Yhat_test is: " + str(np.shape(Yhat_test)))
    # print(Yhat)
    sse_train = f.compute_SSE(Y_train, Yhat_train)
    abs_err_train = f.compute_AbsErr(Y_train, Yhat_train)
    sse_test = f.compute_SSE(Y_test, Yhat_test)
    abs_err_test = f.compute_AbsErr(Y_test, Yhat_test)
    print("sse_train:\t" + str(sse_train))
    print("abser_train:\t" + str(abs_err_train))
    print("sse_test:\t" + str(sse_test))
    print("abser_test:\t" + str(abs_err_test))



#############################################################
#####    MAIN   ######
#############################################################


one_dim()

b_bases = 2
# all_dims_no_interaction(b_bases)
all_dims(b_bases)

#############################################################
#####    MISC   ######
#############################################################


# n = 8
# b = 4
# #test counter_inc():
# ctr_inc = np.zeros(n)
# ct = 0
# while ct < (b**n):
#     print("ct_inc: " + str(ct) + "\t" + str(ctr_inc))
#     f.counter_inc(n, b, ctr_inc)

#     ctr_mod = f.counter_mod(n, b, ct)
#     print("ct_mod: " + str(ct) + "\t" + str(ctr_mod))
#     print("\n")
#     ct += 1
